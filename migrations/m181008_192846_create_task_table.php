<?php

use yii\db\Migration;

/**
 * Handles the creation of table `task`.
 */
class m181008_192846_create_task_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('task', [
            'id' => $this->primaryKey(),
            'time_id' => $this->integer(),
            'user_id' => $this->integer(),
            'task' => $this->string(),
        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('task');
    }
}
