<?php

use yii\db\Migration;

/**
 * Handles the creation of table `time`.
 */
class m181014_171914_create_time_table extends Migration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('time', [
            'id' => $this->primaryKey(),
            'time' => $this->time()->unique(),

        ]);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        $this->dropTable('time');
    }
}
