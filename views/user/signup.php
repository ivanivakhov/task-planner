<?php

use \yii\widgets\ActiveForm;
use \yii\helpers\Html;

$this->title = 'Sign up';

?>

<div class="cover-container jumbotron" align="center" style="background: rgba(217,212,202, 0.7) !important">
    <main role="main" class="inner cover">
        <h1 class="cover-heading">Sign up</h1>
        <br><br>
        <?php $form = ActiveForm::begin(['id' => 'user-signup-form']); ?>
        <?= $form->field($userSignupForm, 'name'); ?>
        <?= $form->field($userSignupForm, 'email'); ?>
        <?= $form->field($userSignupForm, 'password')->passwordInput(); ?>
        <?= $form->field($userSignupForm, 'password2')->passwordInput(); ?>
        <?= Html::submitButton('Create',
            [
                'class' => 'btn btn-danger',
            ]) ?>
        <?php ActiveForm::end(); ?>
    </main>
</div>


