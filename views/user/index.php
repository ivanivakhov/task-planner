<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = Yii::$app->user->getIdentity()->name;
?>

<div class="col-lg-10 col-md-10 col-sm-10 col-xs-12">
    <div class="panel panel-info">
        <div class="panel-heading text-center">
            <h1>Diary</h1>
        </div>
        <div class="panel-body">
            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <tr>
                        <th scope="col" style="width: 15%">time</th>
                        <th scope="col" style="width: 80%">todo</th>
                        <th style="width: 5%"></th>
                    </tr>
                </thead>
                <tbody>
                <?php foreach($taskRecord as $task): ?>
                    <tr>
                        <td scope="row" style="width: 15%">
                            <?php
                                for ($i = 0; $i <= 288; $i++){
                                    if ($timeRecord[$i]->attributes['id'] == $task->attributes['time_id']) {
                                        echo substr($timeRecord[$i]->attributes['time'], 0, -3);
                                    }
                                }
                            ?>
                        </td>
                        <td style="width: 80%"><?=$task->attributes['task']?></td>
                        <td style="width: 5%">
                            <a href="/task/update?id=<?=$task->attributes['id']?>"><span class="glyphicon glyphicon-pencil" aria-hidden="true" style="color: #81a8c8;"></span></a>
                            <a href="/task/view?id=<?=$task->attributes['id']?>"><span class="glyphicon glyphicon-eye-open" aria-hidden="true" style="color: #81a8c8;"></span></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
                </tbody>
            </table>
        </div>
    </div>

</div>

<div class="right-block col-lg-2 col-md-2 col-sm-2 col-xs-12">
    <ul class="list-group" style="min-width: 127px;">
        <li class="list-group-item" style="padding:0;"><a href="#"><div style="padding: 10px 15px;"><i class="glyphicon glyphicon-cloud"></i> dreams</div></a></li>
        <li class="list-group-item" style="padding:0;"><a href="#"><div style="padding: 10px 15px;"><i class="glyphicon glyphicon-blackboard"></i> my skils</div></a></li>
        <li class="list-group-item" style="padding:0;"><a href="#"><div style="padding: 10px 15px;"><i class="glyphicon glyphicon-record"></i> check point</div></a></li>
    </ul>
</div>

