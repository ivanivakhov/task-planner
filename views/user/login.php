<?php

use yii\helpers\Html;
use \yii\widgets\ActiveForm;

$this->title = "Login";
?>

<div class="jumbotron" align="center" style="background: rgba(217,212,202, 0.7) !important">
    <main role="main" class="inner cover">
        <h1 class="cover-heading">Log in</h1>
        <br><br>
        <?php $form = ActiveForm::begin(['id' => 'user-login-form']); ?>
        <?= $form->field($userLoginForm, 'email');?>
        <?= $form->field($userLoginForm, 'password')->passwordInput();?>
        <?= Html::submitButton('Enter', ['class' => 'btn btn-success']);?>
        <?php ActiveForm::end(); ?>
    </main>
</div>



