<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

$this->title = 'Task planner';

?>

<div class="cover-container text-center jumbotron" style="background: rgba(217,212,202, 0.7) !important">
    <main role="main" class="inner cover">
        <h1 class="cover-heading">Welcome</h1>
        <br><br>
        <p class="lead">
            <?= Html::a('Sign up', '/user/signup/', ['class' => 'btn btn-success btn-lg', 'style' => 'width: 200px']); ?>
        </p>
        <p class="lead">
            for creating your own lifestyle <br>
            or
        </p>
        <p class="lead">
            <?= Html::a('Login', '/user/login/', ['class' => 'btn btn-info btn-secondary']); ?>
        </p>
    </main>
</div>

