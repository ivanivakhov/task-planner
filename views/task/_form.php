<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;

/* @var $this yii\web\View */
/* @var $model app\models\TaskRecord */
/* @var $form yii\widgets\ActiveForm */
?>

<div class="task-record-form">

    <?php $form = ActiveForm::begin(); ?>

    <?= $form->field($timeSet, 'time')->textInput(['maxlength' => true, 'type' => 'time']) ?>

    <?= $form->field($model, 'task')->textInput(['maxlength' => true]) ?>

    <div class="form-group">
        <?= Html::submitButton('Save', ['class' => 'btn btn-success']) ?>
    </div>

    <?php ActiveForm::end(); ?>

</div>
