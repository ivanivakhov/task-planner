<?php

use yii\helpers\Html;

/* @var $this yii\web\View */
/* @var $model app\models\TaskRecord */

$this->title = 'Update Task Record: ' . $model->id;
$this->params['breadcrumbs'][] = ['label' => 'Task Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = ['label' => $model->id, 'url' => ['view', 'id' => $model->id]];
$this->params['breadcrumbs'][] = 'Update';
?>
<div class="task-record-update">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'timeSet' => $timeSet,
    ]) ?>

</div>
