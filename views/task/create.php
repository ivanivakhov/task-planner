<?php

use yii\helpers\Html;


/* @var $this yii\web\View */
/* @var $model app\models\TaskRecord */

$this->title = 'Create Task Record';
$this->params['breadcrumbs'][] = ['label' => 'Task Records', 'url' => ['index']];
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="task-record-create">

    <h1><?= Html::encode($this->title) ?></h1>

    <?= $this->render('_form', [
        'model' => $model,
        'timeSet' => $timeSet,
    ]) ?>

</div>
