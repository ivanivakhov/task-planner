<?php

use yii\helpers\Html;
use yii\widgets\ActiveForm;
use \yii\bootstrap\NavBar;
use \yii\bootstrap\Nav;
use app\assets\AppAsset;

AppAsset::register($this);

?>
<?php $this->beginPage() ?>
<!doctype html>
<html lang="<?= Yii::$app->language ?>">
    <head>
        <meta charset="<?= Yii::$app->charset ?>">
        <meta name="viewport"
              content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="ie=edge">
        <title><?= Html::encode($this->title) ?></title>
        <?= Html::csrfMetaTags() ?>
        <?php $this->head(); ?>

    </head>
    <body>
        <?php $this->beginBody(); ?>

        <?php

        if (Yii::$app->user->isGuest) {
            NavBar::begin([
                'brandLabel' => 'Task planner',
                'brandUrl' => Yii::$app->homeUrl,
                'options' => [
                    'class' => 'navbar navbar-default navbar-fixed-top'
                ],
            ]);

            $menu = [
                ['label' => 'Sign up', 'url' => ['/user/signup']],
                ['label' => 'Login', 'url' => ['/user/login']],
            ];
        } else {
            NavBar::begin([
                'brandLabel' => 'Task planner',
                'brandUrl' => '/task/',
                'options' => [
                    'class' => 'navbar navbar-default navbar-fixed-top'
                ],
            ]);

            $menu = [
                ['label' => Yii::$app->user->getIdentity()->name, 'url' => ['/user/index']],
                ['label' => 'Logout', 'url' => ['/user/logout']],
            ];
        }
        echo Nav::widget([
            'options' => ['class' => 'navbar-nav navbar-right'],
            'items' => $menu,
        ]);
        NavBar::end();
        ?>

        <div class="container" style="margin-top: 70px">
            <?= $content; ?>

        </div>

        <?php $this->endBody(); ?>
    </body>
</html>
<?php $this->endPage() ?>