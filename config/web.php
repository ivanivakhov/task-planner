<?php

$config = [
    'id' => 'task-planner',
    'basePath' => realpath(__DIR__ . '/../'),
    'bootstrap' => [
        'debug'
    ],
    'components' => [
        'urlManager' => [
            'enablePrettyUrl' => true,
            'showScriptName' => false,
        ],
        'request' => [
            'cookieValidationKey' => 'super validation key'
        ],
        'db' => require __DIR__ . '/db.php',
        //this gives possibility to antitificate
        'user' => [
            'identityClass' => 'app\models\UserIdentity',
        ],
    ],
    'modules' => [
        'debug' => 'yii\debug\Module'
    ]

];

if (YII_ENV_DEV) {
    $config['bootstrap'][] = 'gii';
    $config['modules']['gii'] = [
        'class' => 'yii\gii\Module',
    ];
}

return $config;
