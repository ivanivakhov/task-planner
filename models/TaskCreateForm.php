<?php

namespace app\models;

use \yii\base\Model;

class TaskCreateForm extends Model
{
    public $id;
    public $time_id;
    public $user_id;
    public $task;

    public function rules ()
    {
        return [
//            ['time', 'required'],
            ['task', 'required'],
        ];
    }

    //do this for filling form automatically from records
    public function setTaskRecord($taskRecord, $timeId)
    {
        $this->id = $taskRecord->id;
        $this->time_id = $timeId;
        $this->user_id = $taskRecord->user_id;
        $this->task = $taskRecord->task;
    }

}