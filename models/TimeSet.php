<?php

namespace app\models;

use yii\base\Model;

class TimeSet extends Model
{
    public $id;
    public $time;

    public function rules ()
    {
        return [
            ['time', 'required'],
        ];
    }

    public function getTimeRecord($timeRecord)
    {
        $this->id = $timeRecord->id;
        $this->time = $timeRecord;
    }

}