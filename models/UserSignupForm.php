<?php

namespace app\models;

use yii\base\Model;

class UserSignupForm extends Model
{
    public $name;
    public $email;
    public $password;
    public $password2;

    public function rules ()
    {
        return [
            ['name', 'required'],
            ['email', 'required'],
            ['password', 'required'],
            ['password2', 'required'],
            ['name', 'string'],
            ['email', 'email', 'message' => 'Incorrect e-mail address'],
            ['password', 'string', 'min' => 5],
            ['password2', 'compare', 'compareAttribute' => 'password'],
            //add validation rule manually
            ['email', 'errorIfEmailUsed'],
        ];
    }

    //do this for filling form automatically from records
    public function setUserRecord($userRecord)
    {
        $this->name = $userRecord->name;
        $this->email = $userRecord->email;
        $this->password = 'qwasd';
        $this->password2 = 'qwasd';
    }

    public function errorIfEmailUsed ()
    {
        //do it if some errors have happened before, e.g. in name field
        if ($this->hasErrors())
            return;
        if (UserRecord::existsEmail($this->email))
            $this->addError('email', 'This e-mail address already exists');
    }

}