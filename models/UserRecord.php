<?php

namespace app\models;

use \yii\db\ActiveRecord;

class UserRecord extends ActiveRecord
{
    public static function tableName ()
    {
        return 'user';
    }

    public function setTestUser ()
    {
        $this->name = "John";
        $this->email = 'ii@mail.net';
        $this->passhash = 'sssss';
        $this->status = '1';
    }

    public static function existsEmail ($email)
    {
        $count = static::find()->where(['email' => $email])->count();
        return $count > 0;
    }

    public static function findUserByEmail ($email)
    {
        return static::findOne(['email' => $email]);
    }

    public function setUserSignupForm($userSignupForm)
    {
        //get info from Signup form and record it into db
        $this->name = $userSignupForm->name;
        $this->email = $userSignupForm->email;
        $this->passhash = $userSignupForm->password;
        $this->status = 1;
    }
}