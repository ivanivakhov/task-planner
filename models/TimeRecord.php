<?php
/**
 * Created by PhpStorm.
 * User: Иван
 * Date: 15.10.2018
 * Time: 16:52
 */

namespace app\models;


use yii\db\ActiveRecord;

class TimeRecord extends ActiveRecord
{
    public static function tableName () {
        return 'time';
    }

//    public function setTimeForm($taskRecord)
//    {
//        $this->id = $taskRecord->time_id;
//        $this->time = $taskRecord->time;
//    }

    public static function findIdByTime ($time)
    {
        return static::find()->where(['time' => $time])->one();
    }
}