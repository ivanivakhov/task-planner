<?php

namespace app\models;

use yii;
use yii\db\ActiveRecord;

class TaskRecord extends ActiveRecord
{
    public static function tableName ()
    {
        return 'task';
    }

    public function setTaskCreateForm($taskCreateForm, $timeId)
    {
        $this->id = $taskCreateForm->id;
        $this->time_id = $timeId;
        $this->user_id = Yii::$app->user->getId();
        $this->task = $taskCreateForm->task;
    }

    public static function getTasksByUserId ($userId)
    {
        return static::find()->where(['user_id' => $userId])->orderBy('time_id')->all();
    }

}