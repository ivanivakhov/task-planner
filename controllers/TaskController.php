<?php

namespace app\controllers;

use Yii;
use app\models\TaskRecord;
use app\models\TaskSearch;
use app\models\TaskCreateForm;
use yii\web\Controller;
use yii\web\NotFoundHttpException;
use yii\filters\VerbFilter;
use app\models\TimeRecord;
use app\models\TimeSet;

/**
 * TaskController implements the CRUD actions for TaskRecord model.
 */
class TaskController extends Controller
{
    /**
     * {@inheritdoc}
     */
    public function behaviors()
    {
        return [
            'verbs' => [
                'class' => VerbFilter::className(),
                'actions' => [
                    'delete' => ['POST'],
                ],
            ],
        ];
    }

    /**
     * Lists all TaskRecord models.
     * @return mixed
     */
    public function actionIndex()
    {
        $userId = Yii::$app->user->getId();
        $taskRecord = TaskRecord::getTasksByUserId($userId);

        $searchModel = new TaskSearch();
        $dataProvider = $searchModel->search($taskRecord);



//        echo '<pre>';
//        var_dump($dataProvider);
        return $this->render('index', [
            'searchModel' => $searchModel,
            'dataProvider' => $dataProvider,
            'taskRecord' => $taskRecord,
        ]);
    }

    /**
     * Displays a single TaskRecord model.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionView($id)
    {

        return $this->render('view', [
            'model' => $this->findModel($id),
        ]);
    }

    /**
     * Creates a new TaskRecord model.
     * If creation is successful, the browser will be redirected to the 'view' page.
     * @return mixed
     */
    public function actionCreate()
    {
        $model = new TaskCreateForm();
        $taskRecord = new TaskRecord();
        $timeSet = new TimeSet();

        if ($model->load(Yii::$app->request->post()) && $timeSet->load(Yii::$app->request->post())) {

            $timeId = TimeRecord::findIdByTime($timeSet->time)->id;

            $taskRecord->setTaskCreateForm($model, $timeId);
            $taskRecord->save();

            return $this->redirect(['view', 'id' => $taskRecord->id]);
        }

        return $this->render('create', [
            'model' => $model,
            'timeSet' => $timeSet,
        ]);
    }

    /**
     * Updates an existing TaskRecord model.
     * If update is successful, the browser will be redirected to the 'view' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionUpdate($id)
    {
        $taskRecord = $this->findModel($id);
        $model = new TaskCreateForm();
        $timeSet = new TimeSet();
        $timeId = TimeRecord::findIdByTime($timeSet->time)->id;

        $model->setTaskRecord($taskRecord, $timeId);
        $timeSet->getTimeRecord('15:25:00');
//        echo '<pre>';
//        var_dump($a::find()->where(['id' => $timeId])->one());
        if ($model->load(Yii::$app->request->post()) && $timeSet->load(Yii::$app->request->post())) {
            $timeId = TimeRecord::findIdByTime($timeSet->time)->id;


            $taskRecord->setTaskCreateForm($model, $timeId);


            $taskRecord->update();
            return $this->redirect(['view', 'id' => $taskRecord->id]);
        }

        return $this->render('update', [
            'model' => $model,
            'timeSet' => $timeSet
        ]);
    }

    /**
     * Deletes an existing TaskRecord model.
     * If deletion is successful, the browser will be redirected to the 'index' page.
     * @param integer $id
     * @return mixed
     * @throws NotFoundHttpException if the model cannot be found
     */
    public function actionDelete($id)
    {
        $this->findModel($id)->delete();

        return $this->redirect('/user/index');
    }

    /**
     * Finds the TaskRecord model based on its primary key value.
     * If the model is not found, a 404 HTTP exception will be thrown.
     * @param integer $id
     * @return TaskRecord the loaded model
     * @throws NotFoundHttpException if the model cannot be found
     */
    protected function findModel($id)
    {
        if (($model = TaskRecord::findOne($id)) !== null) {
            return $model;
        }

        throw new NotFoundHttpException('The requested page does not exist.');
    }
}
