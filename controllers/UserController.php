<?php

namespace app\controllers;

use app\models\UserLoginForm;
use app\models\UserSignupForm;
use yii;
use app\models\UserRecord;
use app\models\TaskRecord;
use app\models\UserIdentity;
use yii\web\Controller;
use app\models\TimeRecord;

class UserController extends Controller
{
    public function actionIndex ()
    {
        $userId = Yii::$app->user->getId();

        $timeRecord = TimeRecord::find()->orderBy('time')->all();
        $taskRecord = TaskRecord::getTasksByUserId($userId);

        return $this->render('index', [
            'taskRecord' => $taskRecord,
            'timeRecord' => $timeRecord,
        ]);
    }

    public function actionSignup ()
    {
        if (Yii::$app->request->isPost)
            return $this->actionSignupPost();

        //i do it for model recognizes this controller
        $userSignupForm = new UserSignupForm();

        $userRecord = new UserRecord();
        $userRecord->setTestUser();
        $userSignupForm->setUserRecord($userRecord);
        return $this->render('signup', compact('userSignupForm'));
    }

    public function actionSignupPost ()
    {
        $userSignupForm = new UserSignupForm();
        if ($userSignupForm->load(Yii::$app->request->post()))
            if ($userSignupForm->validate())
            {
                //record data into db from signup form
                $userRecord = new UserRecord();
                $userRecord->setUserSignupForm($userSignupForm);
                $userRecord->save();
                return $this->redirect('/');
            }
        return $this->render('signup', compact('userSignupForm'));
    }


    public function actionLogin ()
    {
        if (Yii::$app->request->isPost)
            return $this->actionLoginPost();
        $userLoginForm = new UserLoginForm();
        return $this->render('login', compact('userLoginForm'));
    }

    public function actionLoginPost ()
    {
        $userLoginForm = new UserLoginForm();
        if ($userLoginForm->load(Yii::$app->request->post()))
        {
            if ($userLoginForm->validate())
            {
                $userLoginForm->login();
                return $this->redirect('/user/index/');
            }
        }
        return $this->render('login', compact('userLoginForm'));
    }

    public function actionLogout ()
    {
        Yii::$app->user->logout();
        return $this->redirect('/');
    }

}