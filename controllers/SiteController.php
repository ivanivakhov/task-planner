<?php

namespace app\controllers;

use yii\web\Controller;
use yii;

class SiteController extends Controller
{
    public function actionIndex()
    {
        if (Yii::$app->user->isGuest)
            return $this->render('index');
        return $this->redirect('/user/index');
    }
}
